﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using NAudio.Wave;
using NVorbis;

namespace FindBad3dSounds
{
    class Program
    {
        static async Task Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Specify the mod to scan for bad 3d sounds");
                return;
            }
            var modRoot = args.First();
            var sounds = await Get3dSoundsAsync(Path.Combine(modRoot, "Objects"));

            var badSounds = new HashSet<UsedSound>();
            var failedSounds = new HashSet<string>();

            foreach (var sound in sounds)
            {
                Console.WriteLine(sound.SoundFile);
                var file = Path.Combine(modRoot, sound.SoundFile.TrimStart(new []{'/', '\\'}));
                var extension = Path.GetExtension(file);
                if (extension == ".wav")
                {
                    try
                    {
                        using (var reader = new WaveFileReader(file))
                        {
                            if (reader.WaveFormat.Channels > 1)
                            {
                                badSounds.Add(sound);
                            }
                        }

                    }
                    catch(Exception)
                    {
                        failedSounds.Add(file);
                    }
                }
                else if (extension == ".ogg")
                {
                    try
                    {
                        using(var reader = new VorbisReader(file))
                        {
                            if (reader.Channels > 1)
                            {
                                badSounds.Add(sound);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        failedSounds.Add(file);
                    }
                }
                else
                {
                    throw new InvalidOperationException($"Unknown sound format {extension}");
                }
            }

            if (failedSounds.Count == 0 && badSounds.Count == 0)
            {
                Console.WriteLine("No bad sounds!");
                Console.ReadKey();
                return;
            }

            var resultBuilder = new StringBuilder();

            resultBuilder.AppendLine("Non mono 3d sounds:");
            foreach (var sound in badSounds)
            {
                resultBuilder.AppendLine($"- {sound.Sound} ({sound.SoundFile}) in {sound.UsedBy}");
            }

            resultBuilder.AppendLine();
            resultBuilder.AppendLine("Failed checking sounds:");

            foreach (var sound in failedSounds)
            {
                resultBuilder.AppendLine($"- {sound}");
            }

            var resultPath = Path.Combine(modRoot, "Bad3dSounds.txt");
            await File.WriteAllTextAsync(resultPath, resultBuilder.ToString());

            Console.WriteLine($"Found bad sounds. Check {resultPath}");
            Console.ReadKey();
        }


        private static async Task<IEnumerable<UsedSound>> Get3dSoundsAsync(string folder)
        {
            var allSounds = new HashSet<UsedSound>();
            var tweakFiles = new []{"*.con", "*.tweak"}.SelectMany(pattern => Directory.EnumerateFiles(folder, pattern, SearchOption.AllDirectories));

            Console.WriteLine("Collecting sounds...");
            foreach (var file in tweakFiles)
            {
                Console.WriteLine(file);
                using(var fs = File.OpenRead(file))
                using (var reader = new StreamReader(fs))
                {
                    var line = await reader.ReadLineAsync();
                    var is3dSound = false;
                    var soundName = "";
                    var soundFiles = new Collection<string>();
                    while (line != null)
                    {
                        if (IsNewSound(line, out var newSound))
                        {
                            if (is3dSound) {
                                foreach (var soundFile in soundFiles)
                                {
                                    allSounds.Add(new UsedSound(file, soundFile, soundName));
                                }
                            }
                            soundFiles.Clear();
                            is3dSound = false;
                            soundName = newSound;
                        }
                        else if (TryGetIs3dSound(line, out var is3d))
                        {
                            is3dSound = is3d;
                        }
                        else if (TryGetSoundFiles(line, out var files))
                        {
                            foreach(var f in files)
                            {
                                soundFiles.Add(f);
                            }
                        }

                        line = await reader.ReadLineAsync();
                    }

                    // Remaining sounds
                    if (is3dSound) {
                        foreach (var soundFile in soundFiles)
                        {
                            allSounds.Add(new UsedSound(file, soundFile, soundName));
                        }
                    }
                }
            }

            return allSounds;
        }

        private static bool IsNewSound(string line, out string name)
        {
            const string newSoundPattern = @"ObjectTemplate\.activeSafe Sound\s+(?<Value>.*)";
            var m= Regex.Match(line, newSoundPattern, RegexOptions.IgnoreCase);
            if (m.Success)
            {
                name = m.Groups["Value"].Value;
                return true;
            }
            else
            {
                name = null;
                return false;
            }
        }

        private static bool TryGetIs3dSound(string line, out bool is3dSound)
        {
            const string is3dSoundPattern = @"ObjectTemplate\.is3dSound\s+(?<Value>\d+)";
            
            var m = Regex.Match(line, is3dSoundPattern, RegexOptions.IgnoreCase);

            if (m.Success)
            {
                is3dSound = m.Groups["Value"].Value == "1";
                return true;
            }
            else
            {
                is3dSound = false;
                return false;
            }
        }

        private static bool TryGetSoundFiles(string line, out IEnumerable<string> soundFiles)
        {
            const string soundFilesPattern = @"ObjectTemplate\.soundFilename\s+\""(?<Value>.*)\""";
            var m = Regex.Match(line, soundFilesPattern, RegexOptions.IgnoreCase);
            if (m.Success)
            {
                soundFiles = m.Groups["Value"].Value.Split(new []{','}, StringSplitOptions.RemoveEmptyEntries);
                return true;
            }
            else
            {
                soundFiles = Enumerable.Empty<string>();
                return false;
            }
        }
    }
}
