﻿namespace FindBad3dSounds
{
    /// <summary>Found sound with information who uses it</summary>
    public class UsedSound
    {
        /// <summary>Ctor</summary>
        public UsedSound(string usedBy, string soundFile, string sound)
        {
            UsedBy = usedBy;
            SoundFile = soundFile;
            Sound = sound;
        }

        /// <summary>The Sound file</summary>
        public string SoundFile { get;  }

        /// <summary>The Sound</summary>
        public string Sound { get; }

        /// <summary>The file using it</summary>
        public string UsedBy { get;}


        public override string ToString()
        {
            return $"{UsedBy}:{Sound}";
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            UsedSound other = (UsedSound) obj;
            return SoundFile == other.SoundFile && Sound == other.Sound && UsedBy == other.UsedBy;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (SoundFile != null ? SoundFile.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Sound != null ? Sound.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (UsedBy != null ? UsedBy.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}